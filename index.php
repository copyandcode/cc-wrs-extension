<?php

/*
*   Plugin Name: C&C WRS Extension
*   Description: A plugin designed to extend the functionality of the WRS site
*   Version: 1.3.4
*   Author: Copy & Code
*   Author URI: https://copyandcode.co.uk
*   Text Domain: cc-wrs-extension
*/

if(!function_exists('add_action')){
    die("I'm just a plugin, nobody loves me.");
}

// SETUP
define( 'CCWRS_PLUGIN_URL', __FILE__ );

// INCLUDES
include( 'includes/activate.php' );
include( 'includes/admin/init.php' );
include( 'includes/front/enqueue.php' );
include( 'includes/front/display-functions.php' );
include( 'includes/front/user-profile-functions.php' );
include( 'includes/shortcodes/show-user-profile.php' );
include( 'includes/shortcodes/functions.php' );
include( 'process/user-change-profile-pic.php' );
include( 'includes/admin/menus.php' );
include( 'includes/admin/profile-sponsors-options-page.php' );
include( 'process/save-options.php' );

// HOOKS
register_activation_hook( __FILE__, 'ccwrs_activate_plugin' );
add_action( 'admin_init', 'ccwrs_admin_init' );
add_action( 'wp_enqueue_scripts', 'ccwrs_enqueue_scripts' );
add_action( 'wp_ajax_ccwrs_submit_profile_pic_change', 'ccwrs_submit_profile_pic_change' );
add_action( 'admin_menu', 'ccwrs_admin_menus' );

// SHORTCODES
add_shortcode( 'ccwrs_show_user_profile', 'ccwrs_show_user_profile_shortcode' );