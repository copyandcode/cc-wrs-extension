<?php

function ccwrs_submit_profile_pic_change(){

    $output = ['status' => 1 ];

    if( empty($_POST['user_id']) || empty($_POST['user_picture'])){
        wp_send_json( $output );
    }
    
    $user_id = intval($_POST['user_id']);
    $user_picture = intval($_POST['user_picture']);

    if( $user_id=="" || $user_id==NULL || $user_picture=="" || $user_picture==NULL){
        $output['status'] = 2;
        wp_send_json( $output );
    }

    update_user_meta( $user_id, 'wrs_user_profile_pic', $user_picture );

    $output['status'] = 3;

    wp_send_json( $output );

}