<?php

function ccwrs_save_options(){
    if( !current_user_can( 'edit_theme_options' ) ){
        wp_die( 'You are not allowed to access this page.' );
    }

    check_admin_referer( 'ccwrs_options_verify' );

    $ccwrs_opts     =   get_option( 'ccwrs_opts' );
    $ccwrs_opts['profile_sponsor_image_enabled']  =   absint($_POST['ccwrs_profile_sponsor_image_enabled']);
    $ccwrs_opts['profile_custom_sponsor_image_enabled']  =   absint($_POST['ccwrs_profile_custom_sponsor_image_enabled']);
    $ccwrs_opts['profile_override_sponsor_image_enabled']  =   absint($_POST['ccwrs_profile_override_sponsor_image_enabled']);
    $ccwrs_opts['profile_sponsor_image_file_default']  =   absint($_POST['ccwrs_profile_sponsor_image_file_default']);
    $ccwrs_opts['profile_sponsor_image_file_level1']  =   absint($_POST['ccwrs_profile_sponsor_image_file_level1']);
    $ccwrs_opts['profile_sponsor_image_file_level2']  =   absint($_POST['ccwrs_profile_sponsor_image_file_level2']);
    $ccwrs_opts['profile_sponsor_image_file_level3']  =   absint($_POST['ccwrs_profile_sponsor_image_file_level3']);
    $ccwrs_opts['profile_sponsor_image_url_default']  =   $_POST['ccwrs_profile_sponsor_image_url_default'];
    $ccwrs_opts['profile_sponsor_image_url_level1']  =   $_POST['ccwrs_profile_sponsor_image_url_level1'];
    $ccwrs_opts['profile_sponsor_image_url_level2']  =   $_POST['ccwrs_profile_sponsor_image_url_level2'];
    $ccwrs_opts['profile_sponsor_image_url_level3']  =   $_POST['ccwrs_profile_sponsor_image_url_level3'];

    update_option( 'ccwrs_opts', $ccwrs_opts );

    wp_redirect( admin_url( 'admin.php?page=ccwrs_profile_sponsors_opts&status=1' ) );
}