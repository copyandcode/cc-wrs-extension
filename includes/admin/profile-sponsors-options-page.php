<?php

function ccwrs_profile_sponsors_opts_page(){

    ccwrs_enqueue_scripts();
    ccwrs_enqueue_media_upload();

    $ccwrs_opts     =   get_option( 'ccwrs_opts' );

     ?>
        <div class="wrap">
            
            <h3>Profile Page Sponsors</h3>

            <?php
                if( isset($_GET['status']) && $_GET['status']==1 ){
                    ?>
                        <div class="sk-alert sk-alert-success">Options updated successfully</div>
                    <?php
                }
            ?>

            <form method="POST" action="admin-post.php">
                <input type="hidden" name="action" value="ccwrs_save_options">
                <?php wp_nonce_field( 'ccwrs_options_verify' ); ?>

                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Enable sponsor images</label></h4>
                        <p><i>Select <strong>Yes</strong> to enable sponsor banners on user's profile pages.</i></p>
                    </div>
                    <div class="two columns">
                        <p>
                            <select name="ccwrs_profile_sponsor_image_enabled">
                                <option value="0">No</option>
                                <option value="1" <?php echo $ccwrs_opts['profile_sponsor_image_enabled'] == 1 ? 'SELECTED' :''; ?>>Yes</option>
                            </select>
                        </p>
                    </div>
                </div>

                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Enable custom images for levels</label></h4>
                        <p><i>Select <strong>Yes</strong> to show a different logo for each level.<br>Select <strong>No</strong> to show the same logo for all users.</i></p>
                    </div>
                    <div class="two columns">
                        <p>
                            <select name="ccwrs_profile_custom_sponsor_image_enabled">
                                <option value="0">No</option>
                                <option value="1" <?php echo $ccwrs_opts['profile_custom_sponsor_image_enabled'] == 1 ? 'SELECTED' :''; ?>>Yes</option>
                            </select>
                        </p>
                    </div>
                </div>

                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Allow sponsored pages to override settings</label></h4>
                        <p><i>Select <strong>Yes</strong> if you would like School-Sponsored pages to have their logo added instead of the ones selected below.
                        <br>Select <strong>No</strong> to always default to the settings below.</i></p>
                    </div>
                    <div class="two columns">
                        <p>
                            <select name="ccwrs_profile_override_sponsor_image_enabled">
                                <option value="0">No</option>
                                <option value="1" <?php echo $ccwrs_opts['profile_override_sponsor_image_enabled'] == 1 ? 'SELECTED' :''; ?>>Yes</option>
                            </select>
                        </p>
                    </div>
                </div>

                <hr>

                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Default sponsor logo</label></h4>
                        <p><i>If <strong>Custom images for levels</strong> is <strong>disabled</strong> then all user profiles will show this logo.</i></p>
                        <p><i>Images should be 200px x 80px, or equivalent ratio, for best results.</i></p>
                    </div>
                    <div class="six columns">
                        <p><a href="#" id="ccwrs-default-img-upload-btn">Select or Upload Media</a></p>
                        <br>
                        <img id="ccwrs-default-img-preview" <?php echo $ccwrs_opts['profile_sponsor_image_file_default'] !== 0 ? 'src="'.wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_default'] ).'"' :''; ?>>
                        <input type="hidden" name="ccwrs_profile_sponsor_image_file_default" id="ccwrs_default_inputImgID" <?php echo $ccwrs_opts['profile_sponsor_image_file_default'] !== 0 ? 'value="'.$ccwrs_opts['profile_sponsor_image_file_default'].'"' :''; ?>>
                    </div>
                </div>
                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Sponsor URL</label></h4>
                        <p><i>Enter the destination URL for when the user clicks on the logo.</i></p>
                    </div>
                    <div class="six columns">
                        <p><input name="ccwrs_profile_sponsor_image_url_default" <?php echo $ccwrs_opts['profile_sponsor_image_url_default'] !== 0 ? 'value="'.$ccwrs_opts['profile_sponsor_image_url_default'].'"' :''; ?>></p>
                    </div> 
                </div>

                <hr>

                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Level 1 Sponsor</label></h4>
                        <p><i>If <strong>Custom images for levels</strong> is <strong>enabled</strong> this logo will appear on all <strong>Level 1</strong> user profiles.</i></p>
                        <p><i>Images should be 200px x 80px, or equivalent ratio, for best results.</i></p>
                    </div>
                    <div class="six columns">
                        <p><a href="#" id="ccwrs-level1-img-upload-btn">Select or Upload Media</a></p>
                        <br>
                        <img id="ccwrs-level1-img-preview" <?php echo $ccwrs_opts['profile_sponsor_image_file_level1'] !== 0 ? 'src="'.wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_level1'] ).'"' :''; ?>>
                        <input type="hidden" name="ccwrs_profile_sponsor_image_file_level1" id="ccwrs_level1_inputImgID" <?php echo $ccwrs_opts['profile_sponsor_image_file_level1'] !== 0 ? 'value="'.$ccwrs_opts['profile_sponsor_image_file_level1'].'"' :''; ?>>
                    </div>
                </div>
                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Sponsor URL</label></h4>
                        <p><i>Enter the destination URL for when the user clicks on the logo.</i></p>
                    </div>
                    <div class="six columns">
                        <p><input type="text" name="ccwrs_profile_sponsor_image_url_level1" id="ccwrs_level1_inputSponsorLogo" <?php echo $ccwrs_opts['profile_sponsor_image_url_level1'] !== 0 ? 'value="'.$ccwrs_opts['profile_sponsor_image_url_level1'].'"' :''; ?>></p>
                    </div> 
                </div>

                <hr>

                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Level 2 Sponsor</label></h4>
                        <p><i>If <strong>Custom images for levels</strong> is <strong>enabled</strong> this logo will appear on all <strong>Level 2</strong> user profiles.</i></p>
                        <p><i>Images should be 200px x 80px, or equivalent ratio, for best results.</i></p>
                    </div>
                    <div class="six columns">
                        <p><a href="#" id="ccwrs-level2-img-upload-btn">Select or Upload Media</a></p>
                        <br>
                        <img id="ccwrs-level2-img-preview" <?php echo $ccwrs_opts['profile_sponsor_image_file_level2'] !== 0 ? 'src="'.wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_level2'] ).'"' :''; ?>>
                        <input type="hidden" name="ccwrs_profile_sponsor_image_file_level2" id="ccwrs_level2_inputImgID" <?php echo $ccwrs_opts['profile_sponsor_image_file_level2'] !== 0 ? 'value="'.$ccwrs_opts['profile_sponsor_image_file_level2'].'"' :''; ?>>
                    </div>
                </div>
                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Sponsor URL</label></h4>
                        <p><i>Enter the destination URL for when the user clicks on the logo.</i></p>
                    </div>
                    <div class="six columns">
                        <p><input type="text" name="ccwrs_profile_sponsor_image_url_level2" id="ccwrs_level2_inputSponsorLogo" <?php echo $ccwrs_opts['profile_sponsor_image_url_level2'] !== 0 ? 'value="'.$ccwrs_opts['profile_sponsor_image_url_level2'].'"' :''; ?>></p>
                    </div> 
                </div>

                <hr>

                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Level 3 Sponsor</label></h4>
                        <p><i>If <strong>Custom images for levels</strong> is <strong>enabled</strong> this logo will appear on all <strong>Level 3</strong> user profiles.</i></p>
                        <p><i>Images should be 200px x 80px, or equivalent ratio, for best results.</i></p>
                    </div>
                    <div class="six columns">
                        <p><a href="#" id="ccwrs-level3-img-upload-btn">Select or Upload Media</a></p>
                        <br>
                        <img id="ccwrs-level3-img-preview" <?php echo $ccwrs_opts['profile_sponsor_image_file_level3'] !== 0 ? 'src="'.wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_level3'] ).'"' :''; ?>>
                        <input type="hidden" name="ccwrs_profile_sponsor_image_file_level3" id="ccwrs_level3_inputImgID" <?php echo $ccwrs_opts['profile_sponsor_image_file_level3'] !== 0 ? 'value="'.$ccwrs_opts['profile_sponsor_image_file_level3'].'"' :''; ?>>
                    </div>
                </div>
                <div class="sk-row">
                    <div class="five columns">
                        <h4><label>Sponsor URL</label></h4>
                        <p><i>Enter the destination URL for when the user clicks on the logo.</i></p>
                    </div>
                    <div class="six columns">
                        <p><input type="text" name="ccwrs_profile_sponsor_image_url_level3" id="ccwrs_level3_inputSponsorLogo" <?php echo $ccwrs_opts['profile_sponsor_image_url_level3'] !== 0 ? 'value="'.$ccwrs_opts['profile_sponsor_image_url_level3'].'"' :''; ?>></p>
                    </div> 
                </div>

                <hr>

                <div class="sk-row">
                    <p><button type="submit">Save</button></p>
                </div>

            </form>

        </div>
     <?php
}