<?php

function ccwrs_enqueue_scripts(){

    wp_register_style(
        'ccwrs_skeleton', 
        plugins_url( '/assets/styles/skeleton.css', CCWRS_PLUGIN_URL )
    );
    
    wp_register_style(
        'ccwrs_custom', 
        plugins_url( '/assets/styles/ccwrs-custom.css', CCWRS_PLUGIN_URL )
    );

    wp_register_script( 
        'ccwrs_table_sorter', 
        plugins_url( '/assets/scripts/jquery.tablesorter.min.js', CCWRS_PLUGIN_URL),
        array('jquery'),
        '1.0.0',
        true );
    
    wp_register_script( 
        'ccwrs_profile_pic_change', 
        plugins_url( '/assets/scripts/user-change-profile-pic.js', CCWRS_PLUGIN_URL),
        array('jquery'),
        '1.0.0',
        true );

    wp_localize_script( 'ccwrs_profile_pic_change', 'profile_pic_obj', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ) );

    wp_enqueue_style( 'ccwrs_skeleton' );
    wp_enqueue_style( 'ccwrs_custom' );

    wp_enqueue_script( 'ccwrs_table_sorter' );
    wp_enqueue_script( 'ccwrs_profile_pic_change' );    
}

function ccwrs_enqueue_table_css(){
    
    wp_register_style(
        'ccwrs_table_css', 
        plugins_url( '/assets/styles/tables.css', CCWRS_PLUGIN_URL )
    );
    wp_enqueue_style( 'ccwrs_table_css' );
    
}

function ccwrs_enqueue_media_upload(){
    wp_register_script( 
        'ccwrs_media_upload', 
        plugins_url( '/assets/scripts/media-upload.js', CCWRS_PLUGIN_URL),
        array('jquery'),
        '1.0.0',
        true
    );

    wp_enqueue_script( 'ccwrs_media_upload' );
    wp_enqueue_media();
}