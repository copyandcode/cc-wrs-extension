<?php

function getLastPathSegment($url) {

    $path = parse_url($url, PHP_URL_PATH); // to get the path from a whole URL
    $pathTrimmed = trim($path, '/'); // normalise with no leading or trailing slash
    $pathTokens = explode('/', $pathTrimmed); // get segments delimited by a slash

    if (substr($path, -1) !== '/') {

        array_pop($pathTokens);
        
    }

    return end($pathTokens); // get the last segment

}

function check_teacher($id){
    
    global $wpdb; 

    $get_priv = $wpdb->get_var(
        "SELECT umeta_id 
        FROM wp_usermeta
        WHERE meta_key = 'wp_capabilities'
        AND (meta_value 
            LIKE '%teacher%'
            OR meta_value 
            LIKE '%student_manager%')
        AND user_id='$id'
        "
    );
    
    if( $get_priv ){
        return 1;
    }else{
        return 0;
    }

}