<?php

function ccwrs_show_user_profile_shortcode(){

    global $wpdb;
    ccwrs_enqueue_scripts();
    ccwrs_enqueue_table_css();

    $profile_tpl_response = wp_remote_get( plugins_url( 'includes/shortcodes/show-user-profile-template.php', CCWRS_PLUGIN_URL ) );

    $profileHTML = wp_remote_retrieve_body( $profile_tpl_response );

    // check if user is logged in
    $current_user_id = get_current_user_id();
    if( !$current_user_id ){ $loggedin=0; }else{ $loggedin=1; }

    // Get ID from the URI
    $display_id = getLastPathSegment($_SERVER['REQUEST_URI']);
    // if there is no ID in the URI, default to current user
    if( $display_id == 'user-profile' && $loggedin == 1 ){ $display_id = get_current_user_id(); }
    else if( $display_id == 'user-profile' && $loggedin == 0 ){ wp_redirect( '/login' ); exit; }

    // Get display institution
    $display_institution_id = show_user_institution( $display_id );  // outputs as $institution_id
    // Get School Logo
    $display_institution_logo_url = show_institution_logo_url( $display_institution_id );
    // Get Profile Data
    $display_username = show_user_name($display_id);
    $display_institution = show_institution_name($display_institution_id);

    // If user is looking at own profile, add admin links
    $user_admin_output = "";
    if( $display_id == $current_user_id ){

        $user_admin_output .= "<p>";

        if( check_teacher( $current_user_id ) == 0 ){

            $user_admin_output .= "<a onclick=\"wrs_show_profile_pic_change();\">Edit Profile Picture</a> | ";

        }

        $user_admin_output .= "<a href=\"".get_site_url()."/my-account/\">Edit Account</a>";

        $user_admin_output .= "</p>";
    }
    
    // Get VIEWER data (important for priv management)
    $user_institution_id = show_user_institution( $current_user_id );  // outputs as $institution_id
    $user_username = show_user_name($current_user_id);
    $user_institution = show_institution_name($user_institution_id);

    // COURSES, BADGES
    $show_badge_output = "";
    $completed_course_info_output = "";
    // If profile is not a teacher, show Completed Courses and Badges
    if( check_teacher( $display_id ) == 0 ){

        // COMPLETED COURSES
        $completed_course_info_output .= "
            <div class=\"profile-box\" id=\"completed-courses-container\">
                <h3>Completed Courses</h3><br>
        ";  

        $get_completed_courses = $wpdb->get_results("
            SELECT wp_posts.ID, wp_posts.post_title, wp_comments.comment_date
            FROM wp_posts
            INNER JOIN wp_comments ON wp_posts.ID=wp_comments.comment_post_ID
            WHERE wp_comments.user_id='$display_id' AND wp_comments.comment_approved='complete' AND wp_posts.post_type='course'
        ");

        if( $get_completed_courses ){
    
            foreach( $get_completed_courses as $course_info ){
                // get thumbnail
                $get_course_image = $wpdb->get_var("
                    SELECT meta_value FROM wp_postmeta
                    WHERE meta_key='_thumbnail_id' AND post_id='$course_info->ID'
                ");
    
                $completed_course_info_output .= "
                    <div class=\"course-container\">
                        <div class=\"sk-row\">
                            <div class=\"three columns course-image-container\">
                                ".wp_get_attachment_image( $get_course_image, thumbnail, true )."
                            </div>
                            <div class=\"nine columns course-info-container\">
                                <p><strong><a href=\"".esc_url( get_permalink( $course_info->ID ) )."\">".$course_info->post_title."</a></strong></p>
                                <p>Completed: ".readable_date($course_info->comment_date)."</p>
                            </div>
                        </div>
                    </div>
                ";
            }

        }else{ $completed_course_info_output .= "No completed courses yet"; }

        $completed_course_info_output .= "</div>";

        // If user is viewing own profile, or if user is display profile's teacher show courses in progress
        $enrolled_course_info_output = "";
        if( $display_id == $current_user_id || ( check_teacher( $current_user_id ) == 1 && $display_institution==$user_institution )){

            $enrolled_course_info_output .= "
                <div class=\"profile-box\" id=\"enrolled-courses-container\">
                    <h3>Courses In Progress</h3><br>
            ";

            $get_inprogress_courses = $wpdb->get_results("
                SELECT wp_posts.ID, wp_posts.post_title, wp_comments.comment_date FROM wp_posts
                INNER JOIN wp_comments ON wp_posts.ID=wp_comments.comment_post_ID
                WHERE wp_comments.user_id='$display_id' AND wp_comments.comment_approved='in-progress' AND wp_posts.post_type='course'
            ");

            if( $get_inprogress_courses ){
        
                foreach( $get_inprogress_courses as $course_info ){
                    // get course image
                    $get_course_image = $wpdb->get_var("
                        SELECT meta_value FROM wp_postmeta
                        WHERE post_id='$course_info->ID' AND meta_key='_thumbnail_id'
                    ");
        
                    $enrolled_course_info_output .= "
                        <div class=\"course-container\">
                            <div class=\"sk-row\">
                                <div class=\"three columns course-image-container\">
                                    ".wp_get_attachment_image( $get_course_image, thumbnail, true )."
                                </div>
                                <div class=\"nine columns course-info-container\">
                                    <p><strong><a href=\"".esc_url( get_permalink( $course_info->ID ) )."\">".$course_info->post_title."</a></strong></p></h4>
                                    <p>Started: ".readable_date($course_info->comment_date)."</p>
                                </div>
                            </div>
                        </div>
                    ";
                }

            }else{ $enrolled_course_info_output .= "Not currently enrolled on any courses"; }

            $enrolled_course_info_output .= "</div>";
            
        }

        // BADGES
        $show_badge_output .= "
            <div class=\"profile-box\" id=\"earned-badges-container\">
                <h3>Badges</h3>
        ";

        $get_user_badges = $wpdb->get_results("
            SELECT wp_posts.ID, wp_posts.post_title, wp_bp_activity.date_recorded FROM wp_posts 
            INNER JOIN wp_bp_activity ON wp_posts.ID=wp_bp_activity.item_id
            WHERE wp_bp_activity.user_id='$display_id' AND component='badgeos' AND type='activity_update'
            GROUP BY wp_posts.post_title
            ORDER BY wp_bp_activity.date_recorded DESC
        ");

        if( $get_user_badges ){
    
            foreach( $get_user_badges as $show_badge ){
                // get badge image
                $get_badge_image_src = $wpdb->get_var("
                    SELECT meta_value FROM wp_postmeta
                    WHERE meta_key='_thumbnail_id' AND post_id='$show_badge->ID'
                ");

                $badge_image = get_the_post_thumbnail( $get_badge_image_src );
    
                $show_badge_output.="
                    <div class=\"badge-container\">
                        <div class=\"sk-row\">
                            <div class=\"four columns badge-icon-container\">
                                ".wp_get_attachment_image( $get_badge_image_src, thumbnail, true )."
                            </div>
                            <div class=\"eight columns badge-name-container\">
                                <p>
                                    <strong>".$show_badge->post_title."</strong>
                                    <br>
                                    ".readable_date($show_badge->date_recorded)."
                                </p>
                            </div>
                        </div>
                    </div>
                ";
            }
            
        }else{ $show_badge_output .= "No badges yet"; }

        $show_badge_output .= "</div>";
    }

    // Get Points Score
    $user_points_output = "";
    if( check_teacher( $display_id ) == 0 ){

        $get_user_points = $wpdb->get_var("
            SELECT meta_value FROM wp_usermeta
            WHERE user_id='$display_id' AND meta_key='_badgeos_points'
        ");

        if( $get_user_points ){
    
            if( $get_user_points==1 ){ 
                $user_points_output .= "(".$get_user_points." point)";
            }elseif( $get_user_points > 1 ){ 
                $user_points_output .= "(".$get_user_points." points)";
            }else{ 
                $user_points_output .= "(0 points)";
            }

        }

    }

    // PROFILE PIC
    if( check_teacher( $display_id ) == 1 ){
        $profile_image_output = "<img class='user-profile-pic' src='".$display_institution_logo_url."'>";
    }else{

        // get profile pic (or assign one if none listed)
        $user_profile_picture = show_user_profile_pic( $display_id );
        
        $display_user_level = "";
        $level_name_output = "";

        // get all the level IDs
        $get_levels = $wpdb->get_results( "SELECT ID, post_name FROM wp_posts WHERE post_type = 'level' ORDER BY post_name DESC" );

        if( $get_levels ){

            // check if user has level achievement, working from higest level down
            foreach( $get_levels as $show_level ){

                $get_user_level = $wpdb->get_var( "SELECT * FROM wp_bp_activity WHERE user_id='$display_id' AND item_id='$show_level->ID'" );

                if( $get_user_level ){

                    $profile_image_src = plugin_dir_url( CCWRS_PLUGIN_URL )."assets/images/user-profiles/".$user_profile_picture."-".$show_level->post_name.".png";
                    $get_level_name = $wpdb->get_var( "SELECT post_title FROM wp_posts WHERE post_name='$show_level->post_name'" );
                    if( $get_level_name ){ $level_name_output .= $get_level_name; }
                    $display_user_level .= $show_level->post_name;
                    break;

                }

            }

        }

        if( $level_name_output == "" ){
            $profile_image_src = plugin_dir_url( CCWRS_PLUGIN_URL )."assets/images/user-profiles/1-level-1.png";
            $level_name_output .= "Level 0";
            $display_user_level .= "Level 0";
        }

        $profile_image_output = "<img class='user-profile-pic' src='".$profile_image_src."'>";

    }

    // CHANGE PROFILE PIC
    $change_profile_pic_output = "";

    if( check_teacher( $display_id ) == 0 && $display_id == $current_user_id ){
        
        $change_profile_pic_output .= " 
            <div class=\"sk-row\" id=\"user-change-profile-pic-container\">
                <div class=\"eight columns\">
                    <div class=\"profile-box\" id=\"user-change-profile-pic\">
                        <div id=\"change-status\"></div>
                        <form id=\"change_profile_pic_form\">
                            <input type=\"hidden\" id=\"p_userid\" value=\"".$display_id."\">
                    ";

        // get all images for user's level - three options
        for ($x = 1; $x <= 3; $x++) {
            
            $change_profile_pic_output .= "
                            <span class=\"profile_pic_container\">
                                <label for=\"wrs_picture_".$x."\">
                                    <img class=\"wrs_profile_choice\" src=\"".plugin_dir_url( CCWRS_PLUGIN_URL )."assets/images/user-profiles/".$x."-".$show_level->post_name.".png\">
                                    <br>
                                    <input type=\"radio\" id=\"wrs_picture_".$x."\" name=\"wrs_profile_picture\" class=\"profile_pic_check\" value=\"".$x." \">
                                </label>
                            </span>
                    ";
            
        } 

        $change_profile_pic_output .= "
                            <div class=\"profile-pic-buttons\"> 
                                <a onclick=\"wrs_hide_profile_pic_change();\">Cancel</a> <button class=\"profile-pic-button\" id=\"change_profile_pic_button\" type=\"submit\">Change</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        ";

    }

    // If user is a teacher and viewing their own profile, show school leaderboards
    $analysis_output = "";
    $score_table_output = "";    
    $recent_badges_table_output = "";

    if( $display_id == $current_user_id && check_teacher( $current_user_id ) == 1 ){

        $score_table_output .= "<div class=\"sk-row\">";

        $score_table_output .= "
        <div class=\"five columns\">
            <div class=\"dashboard-box\" id=\"student-leaderboard-container\">
                <h3>Student Leaderboard</h3>
                <table class=\"table table-hover\" id=\"student-leaderboard-table\">
                    <thead class=\"text-left\">
                        <tr>
                            <th>Student Name</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
        ";

        $recent_badges_table_output .= "
            <div class=\"seven columns\">
                <div class=\"dashboard-box\" id=\"recent-badges-container\">
                    <h3>Recent Badges</h3>
                    <table class=\"table table-hover\" id=\"recent-badges-table\">
                        <thead class=\"text-left\">
                            <tr>
                                <th>Student Name</th>
                                <th>Badge</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
        ";

        $recent_badges_table_output .= "</div>";
        
        $analysis_output .= "<h5 class=\"analysis-button\"><a href=\"".get_site_url()."/wp-admin/admin.php?page=sensei_analysis\" target=\"_blank\" class=\"analysis-button\">View Full Analysis</a></h5><br>";

        // Get details of all users from current user's institution
        // get all the school users
            
        $lb_get_school_users = $wpdb->get_results("
            SELECT wp_usermeta.user_id FROM wp_usermeta
            WHERE (wp_usermeta.meta_key LIKE 'SCHOOL' AND wp_usermeta.meta_value='$current_user_id')
            AND user_id IN (
                SELECT wp_usermeta.user_id FROM wp_usermeta
                WHERE wp_usermeta.meta_key = 'wp_capabilities' AND wp_usermeta.meta_value LIKE '%subscriber%'
                )
        ");

        $lb_get_students = $wpdb->get_results(" 
            SELECT wp_users.display_name, wp_users.ID, wp_usermeta.user_id, wp_usermeta.meta_value 
            FROM wp_usermeta INNER JOIN wp_users ON wp_usermeta.user_id=wp_users.ID
            WHERE wp_usermeta.user_id IN (
                SELECT wp_usermeta.user_id 
                FROM wp_usermeta
                WHERE wp_usermeta.meta_key = 'wp_capabilities' AND wp_usermeta.meta_value LIKE '%subscriber%'
                    AND wp_usermeta.user_id IN (
                        SELECT wp_usermeta.user_id 
                        FROM wp_usermeta
                        WHERE (wp_usermeta.meta_key='SCHOOL' AND wp_usermeta.meta_value='$current_user_id')
                    )
                )
                AND meta_key='_badgeos_points'
            ORDER BY wp_usermeta.meta_value DESC
        ");

        // Get score details of all students
        if( $lb_get_students ){
            foreach( $lb_get_students as $lb_student ){
                $score_table_output.="
                    <tr><td><a href=\"".get_site_url()."/user-profile/".$lb_student->ID."\" target=\"_blank\">".$lb_student->display_name."</a></td><td>".$lb_student->meta_value."</td></tr>
                ";
            }
        }

        // Get the latest badge winners
        $lb_get_recent_badge_awards = $wpdb->get_results("
            SELECT wp_users.ID, wp_users.display_name, wp_posts.post_title, wp_bp_activity.date_recorded
            FROM wp_bp_activity
            INNER JOIN wp_users ON wp_bp_activity.user_id=wp_users.ID 
            INNER JOIN wp_posts ON wp_bp_activity.item_id=wp_posts.ID 
            WHERE wp_bp_activity.component='badgeos' AND wp_bp_activity.type='activity_update'
            AND user_id IN (
                SELECT wp_usermeta.user_id 
                FROM wp_usermeta
                WHERE (wp_usermeta.meta_key='SCHOOL' AND wp_usermeta.meta_value='$current_user_id')
                AND user_id IN (
                    SELECT wp_usermeta.user_id 
                    FROM wp_usermeta
                    WHERE wp_usermeta.meta_key = 'wp_capabilities' AND wp_usermeta.meta_value LIKE '%subscriber%'
                ) 
            )
            ORDER BY date_recorded DESC
            LIMIT 5
        ");
        
        if( $lb_get_recent_badge_awards ){
            foreach( $lb_get_recent_badge_awards as $lb_show_recent_badges ){
                $recent_badges_table_output.="
                    <tr><td><a href=\"".get_site_url()."/user-profile/".$lb_show_recent_badges->ID."\" target=\"_blank\">".$lb_show_recent_badges->display_name."</a></td><td>".$lb_show_recent_badges->post_title."</td><td>".readable_date($lb_show_recent_badges->date_recorded)."</td></tr>
                ";
            }
        }

        $score_table_output .= "
                        </tbody>
                    </table>
                </div> 
            </div>
        ";

        $recent_badges_table_output .= "
                        </tbody>
                    </table>
                </div> 
            </div>
        ";

    }

    // SHOW SPONSOR IMAGE
    $ccwrs_opts     =   get_option( 'ccwrs_opts' );
    $sponsor_image_active = $ccwrs_opts['profile_sponsor_image_enabled'];
    $sponsor_image_custom_enabled = $ccwrs_opts['profile_custom_sponsor_image_enabled'];
    $sponsor_image_override_enabled = $ccwrs_opts['profile_override_sponsor_image_enabled'];

    $sponsor_image_output = "";

    
    // If user is looking at own profile, and if the sponsor image is active, then display the image
    if( $display_id == $current_user_id && $sponsor_image_active == 1 ){

        $sponsor_image_output   .=  "<div class=\"four columns\" id=\"profile-sponsor-container\">";

        // Check if Sponsor override is enabled
        if( $sponsor_image_override_enabled == "1" ){
            
            // Check if user is assigned to a sponsored school
            $get_school_sponsor = $wpdb->get_var("
               SELECT meta_value FROM wp_postmeta
               WHERE post_id = '$user_institution_id' AND meta_key = '_wrg_schools_sponsor_primary'
               LIMIT 1 
            ");
            
            if( $get_school_sponsor != "" || $get_school_sponsor !== NULL ){
                //get logo
                $get_sponsor_logo_url = $wpdb->get_var("
                    SELECT meta_value FROM wp_postmeta
                    WHERE post_id = '$get_school_sponsor' AND meta_key = 'logo'
                    LIMIT 1
                ");
                $get_sponsor_url = $wpdb->get_var("
                    SELECT meta_value FROM wp_postmeta
                    WHERE post_id = '$get_school_sponsor' AND meta_key = 'url_logo'
                    LIMIT 1
                ");
                if( $get_sponsor_url == "" || $get_sponsor_url == NULL ){
                    $sponsor_image_output .= "<img class=\"profile-sponsor-image\" src=\"".$get_sponsor_logo_url."\">";
                }else{
                    $sponsor_image_output .= "
                        <a href=\"".$get_sponsor_url."\">
                            <img class=\"profile-sponsor-image\" src=\"".$get_sponsor_logo_url."\">
                        </a>
                        ";
                }
            }else{
                // Check if image is default or level based
                if( $sponsor_image_custom_enabled == "1" ){
                    // Check levels
                    if( $display_user_level == "Level 0" ){
                        $sponsor_image_output   .=   "
                            <a href=\"".$ccwrs_opts['profile_sponsor_image_url_default']."\">
                                <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_default'] )."\">
                            </a>
                            ";
                    }elseif( $display_user_level == "level-1" ){
                        $sponsor_image_output   .=   "
                            <a href=\"".$ccwrs_opts['profile_sponsor_image_url_level1']."\">
                                <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_level1'] )."\">
                            </a>
                            ";
                    }elseif( $display_user_level == "level-2" ){
                        $sponsor_image_output   .=   "
                            <a href=\"".$ccwrs_opts['profile_sponsor_image_url_level2']."\">
                                <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_level2'] )."\">
                            </a>
                            ";
                    }elseif( $display_user_level == "level-3" ){
                        $sponsor_image_output   .=   "
                            <a href=\"".$ccwrs_opts['profile_sponsor_image_url_level3']."\">
                                <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_level3'] )."\">
                            </a>
                            ";
                    }else{
                        $sponsor_image_output   .=   "
                            <a href=\"".$ccwrs_opts['profile_sponsor_image_url_default']."\">
                                <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_default'] )."\">
                            </a>
                            ";
                    }
                }else{
                    $sponsor_image_output   .=   "
                        <a href=\"".$ccwrs_opts['profile_sponsor_image_url_default']."\">
                            <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_default'] )."\">
                        </a>
                        ";
                }

            }

        }else{ // Otherwise, continue checks

            // Check if image is default or level based
            if( $sponsor_image_custom_enabled == "1" ){
                // Check levels
                if( $display_user_level == "Level 0" ){
                    $sponsor_image_output   .=   "
                        <a href=\"".$ccwrs_opts['profile_sponsor_image_url_default']."\">
                            <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_default'] )."\">
                        </a>
                        ";
                }elseif( $display_user_level == "level-1" ){
                    $sponsor_image_output   .=   "
                        <a href=\"".$ccwrs_opts['profile_sponsor_image_url_level1']."\">
                            <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_level1'] )."\">
                        </a>
                        ";
                }elseif( $display_user_level == "level-2" ){
                    $sponsor_image_output   .=   "
                        <a href=\"".$ccwrs_opts['profile_sponsor_image_url_level2']."\">
                            <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_level2'] )."\">
                        </a>
                        ";
                }elseif( $display_user_level == "level-3" ){
                    $sponsor_image_output   .=   "
                        <a href=\"".$ccwrs_opts['profile_sponsor_image_url_level3']."\">
                            <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_level3'] )."\">
                        </a>
                        ";
                }else{
                    $sponsor_image_output   .=   "
                        <a href=\"".$ccwrs_opts['profile_sponsor_image_url_default']."\">
                            <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_default'] )."\">
                        </a>
                        ";
                }
            }else{
                $sponsor_image_output   .=   "
                    <a href=\"".$ccwrs_opts['profile_sponsor_image_url_default']."\">
                        <img class=\"profile-sponsor-image\" src=\"".wp_get_attachment_url( $ccwrs_opts['profile_sponsor_image_file_default'] )."\">
                    </a>
                    ";
            }
        
            $sponsor_image_output   .=  "</div>";
        }
    }
    
    
    // OUTPUT ALL STR_REPLACES

    $profileHTML = str_replace( 'USER_NAME_PH', $display_username, $profileHTML );
    $profileHTML = str_replace( 'USER_INSTITUTION_PH', $display_institution, $profileHTML );$profileHTML = str_replace( 'USER_NAME_PH', $display_username, $profileHTML );
    $profileHTML = str_replace( 'USER_INSTITUTION_PH', $display_institution, $profileHTML );


    $profileHTML = str_replace( 'USER_PROFILE_PIC_PH', $profile_image_output, $profileHTML );
    $profileHTML = str_replace( 'USER_LEVEL_PH', $level_name_output, $profileHTML );
    $profileHTML = str_replace( 'USER_POINTS_PH', $user_points_output, $profileHTML );
    
    $profileHTML = str_replace( 'USER_ADMIN_PH', $user_admin_output, $profileHTML );
    $profileHTML = str_replace( 'USER_CHANGE_PROFILE_PIC_PH', $change_profile_pic_output, $profileHTML );

    $profileHTML = str_replace( 'COMPLETED_COURSE_LIST_PH', $completed_course_info_output, $profileHTML );
    $profileHTML = str_replace( 'ENROLLED_COURSE_LIST_PH', $enrolled_course_info_output, $profileHTML );
    $profileHTML = str_replace( 'SHOW_BADGES_LIST_PH', $show_badge_output, $profileHTML );

    $profileHTML = str_replace( 'ANALYSIS_PH', $analysis_output, $profileHTML );
    $profileHTML = str_replace( 'LB_RECENT_BADGES_PH', $recent_badges_table_output, $profileHTML );
    $profileHTML = str_replace( 'LB_SCORE_TABLE_PH', $score_table_output, $profileHTML );
    
    $profileHTML = str_replace( 'SPONSOR_IMAGE_PH', $sponsor_image_output, $profileHTML );
    
    return $profileHTML;

}