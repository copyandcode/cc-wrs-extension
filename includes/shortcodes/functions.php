<?php

function show_user_name( $userID ){
    global $wpdb;

    $user_name = $wpdb->get_var("
        SELECT display_name FROM wp_users
        WHERE ID = '$userID'
        LIMIT 1
    ");

    return $user_name;
}

function show_user_profile_pic( $userID ){
    global $wpdb;

    $user_profile_picture = $wpdb->get_var("
        SELECT meta_value FROM wp_usermeta 
        WHERE user_id='$userID' AND meta_key='wrs_user_profile_pic'
        ");
    if( !$user_profile_picture ){
        // assign one
        $user_profile_picture = rand(1,3);
        update_user_meta( $display_id, 'wrs_user_profile_pic', $user_profile_picture );
    }
    
    return $user_profile_picture;
}

function show_user_institution( $userID ){
    global $wpdb;

    $institution_id = $wpdb->get_var("
        SELECT meta_value FROM wp_usermeta
        WHERE user_id='$userID' AND meta_key LIKE 'School'
    ");

    return $institution_id;
}

function show_institution_name( $institutionID ){
    global $wpdb;

    $institution_name = $wpdb->get_var("
        SELECT post_title FROM wp_posts
        WHERE ID = '$institutionID'
        LIMIT 1
    ");

    return $institution_name;
}

function show_institution_logo_url( $institutionID ){
    global $wpdb;

    $institution_logo_url = $wpdb->get_var("
        SELECT meta_value FROM wp_postmeta
        WHERE post_id='$institutionID' AND meta_key='wrg_logo'
        LIMIT 1
    ");

    return $institution_logo_url;
}

?>