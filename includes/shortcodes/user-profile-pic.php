<?php

// get existing profile pic

$user_profile_picture = $wpdb->get_var(
    "SELECT meta_value FROM wp_usermeta WHERE user_id='$display_id' AND meta_key='wrs_user_profile_pic'"
);
if( !$user_profile_picture ){
    // assign one
    $user_profile_picture = rand(1,3);
    update_user_meta( $display_id, 'wrs_user_profile_pic', $user_profile_picture );
}else{
    $user_profile_picture=$user_profile_picture;
}




// change profile pic
$change_profile_pic_output = "";
if( $display_id == $current_user_id ){

    $change_profile_pic_output .= " 
        <div class=\"sk-row\" id=\"user-change-profile-pic-container\">
            <div class=\"eight columns\">
                <div class=\"profile-box\" id=\"user-change-profile-pic\">
                    <div id=\"change-status\"></div>
                    <form id=\"change_profile_pic_form\">
                        <input type=\"hidden\" id=\"p_userid\" value=\"".$display_id."\">
                ";

    // get all images for user's level - three options
    for ($x = 1; $x <= 3; $x++) {
        
        $change_profile_pic_output .= "
                        <span class=\"profile_pic_container\">
                            <label for=\"wrs_picture_".$x."\">
                                <img class=\"wrs_profile_choice\" src=\"".plugin_dir_url( CCWRS_PLUGIN_URL )."assets/images/user-profiles/".$x."-".$show_level->post_name.".png\">
                                <br>
                                <input type=\"radio\" id=\"wrs_picture_".$x."\" name=\"wrs_profile_picture\" class=\"profile_pic_check\" value=\"".$x." \">
                            </label>
                        </span>
                ";
        
    } 

    $change_profile_pic_output .= "
                        <div class=\"profile-pic-buttons\"> 
                            <a onclick=\"wrs_hide_profile_pic_change();\">Cancel</a> <button class=\"profile-pic-button\" id=\"change_profile_pic_button\" type=\"submit\">Change</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    ";
}
$profileHTML = str_replace( 'USER_CHANGE_PROFILE_PIC_PH', $change_profile_pic_output, $profileHTML );



?>