<?php

function ccwrs_activate_plugin(){
    if( version_compare( get_bloginfo( 'version' ), '4.5', '<' ) ){
        wp_die( __('You must update Wordpress to use this plugin', 'cc-wrs-extension') );
    }

    //ccwrs_init();
    //flush_rewrite_rules();

    $ccwrs_opts = get_option( 'ccwrs_opts' );

    if( !$ccwrs_opts ){
        $opts = [
            'profile_sponsor_image_enabled'           =>  0,
            'profile_custom_sponsor_image_enabled'    =>  0,
            'profile_override_sponsor_image_enabled'    =>  0,
            'profile_sponsor_image_file_default'      =>  0,
            'profile_sponsor_image_file_level1'       =>  0,
            'profile_sponsor_image_file_level2'       =>  0,
            'profile_sponsor_image_file_level3'       =>  0,
            'profile_sponsor_image_url_default'       =>  "",
            'profile_sponsor_image_url_level1'        =>  "",
            'profile_sponsor_image_url_level2'        =>  "",
            'profile_sponsor_image_url_level3'        =>  ""
        ];

        add_option( 'ccwrs_opts', $opts );
    }

}