=== CC-WRS-Extension Plugin ===
A plugin designed to extend the functionality of the Work Ready Schools Platform

=== Plugin Requirements ===
* wrg-schools plugin (and all its dependencies)
* BadgeOS
* BadgeOS Community Add-On
* BadgeOS Sensei Add-On
* BuddyPress
* BuddyPress for Sensei

=== Installation ===
1. Installation handled through Composer (wrg-schools plugin as master)
2. Activate all other required plugins first
3. Then activate the CC-WRS-Extension
4. To ensure no clashes with the existing WRG plugin, disable default 'Register' and 'Activate' pages on the BuddyPress Plugin (Settings > BuddyPress > Pages)

=== Updates ===

== Version 1.3.4 Update ==

* Removed order list from profile page

== Version 1.3.3 Update ==

* Code cleanup (added includes/shortcodes/functions.php)
* Removed redundant files
* Removed ccwrs_show_school_leaderboard shortcode functionality
* Added uninstall cleanup

== Version 1.3.2 Update ==

User Profile Sponsors
* Enabled option to override sponsor images for sponsored schools
* (Plugin may need to be deactivated and reactivated after update)

== Version 1.3.1 Update ==

User Profile Sponsors
* Added sponsor URLs to logos

== Version 1.3 Update ==

User Profile Sponsors
* Added the option of displaying a sponsor logo on users' profiles
* Includes an admin menu, and writes to the wp_options table

== Version 1.2 Update ==

* Added login redirect for pages requiring user access
* Edited spacing on User Profile pages
* Removed 5 badge limit on Recent Badges
* Fixed bug where some users were showing as both Level 0 and Level 1
* Fixed bug where some course links weren't redirecting properly
* Removed some commented code

== Version 1.1 Update ==

Student User View
* Removed the Chapters from the Course Progress table on the Student User Profile
* Added 'Level 0' to profile picture function to allow brand new users to have a profile picture and level

Teacher User View
* Edited text on Template to differentiate the Leaderboard and Recent Badges tables
* Edited the leaderboard table query to display data in correct order