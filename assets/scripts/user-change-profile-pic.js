jQuery(function($){
    
    $("#change_profile_pic_form").on( "submit", function(e){
        e.preventDefault();

        $(this).hide();
        //$("#recipe-status").html('<div class="alert alert-info text-center">Please wait!</div>');

        var form                =   {
            action:                 "ccwrs_submit_profile_pic_change",
            user_id:                $("#p_userid").val(),
            user_picture:           $(".profile_pic_check:checked").val()
        };

        $("#change-status").html('<div class="alert alert-info text-center">Changing picture...</div>');

        $.post( profile_pic_obj.ajax_url, form ).always(function(data){
            
            if( data.status == 3 ){
                console.log("Success!");
                $("#change-status").html('<div class="alert alert-info text-center">Picture changed!</div>');
                location.reload(true);
            }else{
                console.log("Fail other");
                $("#change-status").html('<div class="alert alert-info text-center">There was an error. Please try again later.</div>');                
            }
            
        });
    });

});

function wrs_show_profile_pic_change(){
    document.getElementById('user-change-profile-pic-container').style.display='block';
}
function wrs_hide_profile_pic_change(){
    document.getElementById('user-change-profile-pic-container').style.display='none';
}