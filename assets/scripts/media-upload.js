jQuery(function($){
    
    // Default
    
    var default_sponsor_logo      =   wp.media({
        title:      'Select or Upload Default Media',
        button:     {
            text:   'Use this media'
        },
        multiple:   false
    });

    $("#ccwrs-default-img-upload-btn").on('click', function(e){
        e.preventDefault();
        default_sponsor_logo.open();
    });

    default_sponsor_logo.on('select', function(){
        var attachment      =   default_sponsor_logo.state().get( 'selection' ).first().toJSON();
        $("#ccwrs-default-img-preview").attr( 'src', attachment.url );
        $("#ccwrs_default_inputImgID").val( attachment.id );
    });

    // Level 1

    var level1_sponsor_logo      =   wp.media({
        title:      'Select or Upload Level 1 Media',
        button:     {
            text:   'Use this media'
        },
        multiple:   false
    });

    $("#ccwrs-level1-img-upload-btn").on('click', function(e){
        e.preventDefault();
        level1_sponsor_logo.open();
    });

    level1_sponsor_logo.on('select', function(){
        var attachment      =   level1_sponsor_logo.state().get( 'selection' ).first().toJSON();
        $("#ccwrs-level1-img-preview").attr( 'src', attachment.url );
        $("#ccwrs_level1_inputImgID").val( attachment.id );
    });

    // Level 2

    var level2_sponsor_logo      =   wp.media({
        title:      'Select or Upload Level 2 Media',
        button:     {
            text:   'Use this media'
        },
        multiple:   false
    });

    $("#ccwrs-level2-img-upload-btn").on('click', function(e){
        e.preventDefault();
        level2_sponsor_logo.open();
    });

    level2_sponsor_logo.on('select', function(){
        var attachment      =   level2_sponsor_logo.state().get( 'selection' ).first().toJSON();
        $("#ccwrs-level2-img-preview").attr( 'src', attachment.url );
        $("#ccwrs_level2_inputImgID").val( attachment.id );
    });

    // Level 3

    var level3_sponsor_logo      =   wp.media({
        title:      'Select or Upload Level 3 Media',
        button:     {
            text:   'Use this media'
        },
        multiple:   false
    });

    $("#ccwrs-level3-img-upload-btn").on('click', function(e){
        e.preventDefault();
        level3_sponsor_logo.open();
    });

    level3_sponsor_logo.on('select', function(){
        var attachment      =   level3_sponsor_logo.state().get( 'selection' ).first().toJSON();
        $("#ccwrs-level3-img-preview").attr( 'src', attachment.url );
        $("#ccwrs_level3_inputImgID").val( attachment.id );
    });

});